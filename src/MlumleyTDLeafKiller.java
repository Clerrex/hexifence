import java.io.IOException;
import java.io.PrintStream;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;

import aiproj.hexifence.Move;
import aiproj.hexifence.Piece;
import aiproj.hexifence.Player;
import aiproj.hexifence.mlumley.Board;
import aiproj.hexifence.mlumley.Coordinates;
import aiproj.hexifence.mlumley.Stats;
import aiproj.hexifence.mlumley.Tile;

/**
 * AI player for HexiFence using Negamax with alpha-beta pruning and move
 * ordering
 */
public class MlumleyTDLeafKiller implements Player, Piece {

	private int myPiece = 0;
	private int opponentPiece = 0;
	private Board board = null;
	private boolean validMove = true;
	private Stats stats = new Stats();
	private Move bestMove = null;
	private Random r = null;
	private int numberOfMoves = 0;

	private Move[][] killerMoves = new Move[80][2];
	private int ply = 0;

	// TDLeaf
	private double[] weights = { 0.5842497648669704, -2.298235298405469, -0.7846338821051938, -27.339027183736697,
			-20.05480307565583, -10.210708931946138, -2.7930263677496487, 0.549486694471807 };
	private ArrayList<Board> states = new ArrayList<Board>();
	private ArrayList<Double> diff = new ArrayList<Double>();
	private boolean called = false;
	private Board principalLeaf = null;
	private double principalScore = -Double.MAX_VALUE;

	@Override
	public int init(int n, int p) {
		board = new Board(n);
		board.createBoard();
		r = new Random();
		ply = 0;

		// TDLeaf
		called = false;
		numberOfMoves = 0;
		// states.add(new Board(this.board));
		principalLeaf = null;
		principalScore = -Double.MAX_VALUE;

		if (p == 1) {
			myPiece = Piece.BLUE;
			opponentPiece = Piece.RED;
		} else if (p == 2) {
			myPiece = Piece.RED;
			opponentPiece = Piece.BLUE;
		} else {
			return -1;
		}
		return 0;
	}

	@Override
	public Move makeMove() {
		if (numberOfMoves < 0) {
			numberOfMoves++;
			Move move = new Move();
			int row = r.nextInt(board.getLength());
			int col = r.nextInt(board.getLength());

			while (board.getBoard()[row][col] != '+') {
				row = r.nextInt(board.getLength());
				col = r.nextInt(board.getLength());
			}

			// Don't pick tiles with two open edges
			// Will get stuck if only 2 open edge tile available
			for (int i = 0; i < board.getTiles().size(); i++) {
				if (board.getTiles().get(i).numOpenEdges() == 2
						&& board.getTiles().get(i).containsPoint(new Coordinates(row, col))) {
					i = 0;
					while (board.getBoard()[row][col] != '+') {
						row = r.nextInt(board.getLength());
						col = r.nextInt(board.getLength());
					}
				}
			}

			for (Tile tile : board.getTiles()) {
				if (tile.numOpenEdges() == 1) {
					for (Coordinates point : tile.getPoints()) {
						if (board.getValue(point.getX(), point.getY()) == '+') {
							row = point.getX();
							col = point.getY();
						}
					}
				}
			}
			move.P = myPiece;
			move.Row = row;
			move.Col = col;

			board.setChar(move, myPiece);

			board.didCapture(new Coordinates(move.Row, move.Col), myPiece);
			ply++;

			// TDLeaf
			// states.add(new Board(this.board));

			return move;
		} else {

			this.bestMove = null;
			negamax(this.board, myPiece, 4, ply, -Double.MAX_VALUE, Double.MAX_VALUE);
			ply++;
			board.setChar(this.bestMove, this.myPiece);
			board.didCapture(new Coordinates(this.bestMove.Row, this.bestMove.Col), myPiece);

			// TDLeaf
			states.add(new Board(this.principalLeaf));
			principalLeaf = null;
			principalScore = -Double.MAX_VALUE;

			return this.bestMove;
		}
	}

	@Override
	public int opponentMove(Move m) {
		if (board.getValue(m) == '+') {
			board.setChar(m, this.opponentPiece);
			if (board.didCapture(new Coordinates(m.Row, m.Col), opponentPiece)) {

				// TDLeaf
				// states.add(new Board(this.board));

				return 1;
			}

			// TDLeaf
			// states.add(new Board(this.board));

			return 0;
		}
		validMove = false;
		return -1;
	}

	@Override
	public int getWinner() {
		int winner = 0;

		if (!validMove) {
			return Piece.INVALID;
		} else if ((winner = board.findWinner()) > 0) {

			// TDLeaf
			if (!called) {
				called = true;
				calcDiff();
				tdUpdate();
				printWeights();
			}

			return winner;
		}
		return 0;
	}

	@Override
	public void printBoard(PrintStream output) {
		for (char[] line : board.getBoard()) {
			for (char ch : line) {
				System.out.print(ch + " ");
			}
			System.out.print("\n");
		}
	}

	/**
	 * Depth limited negamax with alpha-beta pruning and move ordering. The best
	 * move is stored in bestMove, an object variable
	 * 
	 * @param board
	 *            the board
	 * @param piece
	 *            the player
	 * @param depth
	 *            the cutoff depth
	 * @param alpha
	 *            the alpha value
	 * @param beta
	 *            the beta value
	 * @return the best score
	 */
	private double negamax(Board board, int piece, int depth, int ply, double alpha, double beta) {

		List<Move> nextMoves = generateMoves(board, piece);
		double score;
		double bestScore = -Double.MAX_VALUE;

		// Reached end of tree or cutoff depth
		if (depth == 0 || nextMoves.isEmpty()) {
			score = evaluateBoard(board, piece, weights);
			if (score > this.principalScore) {
				this.principalLeaf = new Board(board);
				this.principalScore = score;
			}
			return score;
		}

		nextMoves = orderMoves((LinkedList<Move>) nextMoves, ply);

		// For each child move calculate its score
		for (Move move : nextMoves) {
			board.setChar(move, piece);
			// board.didCapture(new Coordinates(move.Row, move.Col), piece);
			if (piece == this.myPiece) {
				score = -negamax(board, opponentPiece, depth - 1, ply + 1, -beta, -alpha);
			} else {
				score = -negamax(board, myPiece, depth - 1, ply + 1, -beta, -alpha);
			}

			// Undo the move so that other moves can be made
			board.undoMove(move);

			if (bestScore < score) {
				bestScore = score;
				this.bestMove = move;
			}
			alpha = Math.max(alpha, score);
			if (alpha >= beta) {
				this.addKillerMove(move, ply);
				return beta;
			}
		}
		return bestScore;
	}

	private void addKillerMove(Move move, int ply) {
		for (int i = killerMoves[ply].length - 2; i >= 0; i--) {
			killerMoves[ply][i + 1] = killerMoves[ply][i];
		}
		killerMoves[ply][0] = move;
	}

	private List<Move> orderMoves(LinkedList<Move> nextMoves, int ply) {
		Move killerMove = null;

		for (int slot = 0; slot < killerMoves[ply].length; slot++) {
			if ((killerMove = killerMoves[ply][slot]) != null) {

				for (int i = 0; i < nextMoves.size(); i++)
					if (nextMoves.get(i).Row == killerMove.Row && nextMoves.get(i).Col == killerMove.Col) {
						Move temp = nextMoves.remove(i);
						nextMoves.addFirst(temp);
					}
			}
		}
		return nextMoves;
	}

	private double evaluateBoard(Board board, int piece, double[] weights) {
		double score = 0;
		stats.setBoard(board);

		score += weights[0] * stats.numCapturableHexes();
		score += weights[1] * stats.maxCapturableHexes();

		score += weights[2] * stats.numCapturedTiles();

		score += weights[3] * stats.tilesWithTwoOpenEdges();
		score += weights[4] * stats.tilesWithThreeOpenEdges();
		score += weights[5] * stats.tilesWithFourOpenEdges();
		score += weights[6] * stats.tilesWithFiveOpenEdges();
		score += weights[7] * stats.tilesWithSixOpenEdges();

		return score;
	}

	/**
	 * Given a board determine all possible moves
	 * 
	 * @param board
	 *            the board
	 * @param piece
	 *            the player
	 * @return A list of moves
	 */
	private List<Move> generateMoves(Board board, int piece) {
		List<Move> moves = new LinkedList<Move>();
		for (int i = 0; i < board.getLength(); i++) {
			for (int j = 0; j < board.getLength(); j++) {
				if (board.getValue(i, j) == '+') {
					Move move = new Move();
					if (piece == Piece.BLUE)
						move.P = Piece.BLUE;
					else
						move.P = Piece.RED;
					move.Row = i;
					move.Col = j;
					moves.add(move);
				}
			}
		}
		return moves;
	}

	// TDLeaf

	private void tdUpdate() {
		double rate = 0.4;
		for (int i = 0; i < weights.length; i++) {
			weights[i] = weights[i] + rate * firstSum(i);
		}
		clearTD();
	}

	private double reward(Board board) {
		int winner = 0;
		if ((winner = board.findWinner()) > 0) {
			if (myPiece == winner) {
				return 1;
			} else {
				return -1;
			}
		}
		return 0;
	}

	private double reward(Board board, double[] weights) {
		double score = this.evaluateBoard(board, this.myPiece, weights);
		return Math.tanh(score);
	}

	private double reward(Board board, double weight, double delta) {
		double[] tempWeights = new double[this.weights.length];
		for (int i = 0; i < this.weights.length; i++) {
			if (i == weight) {
				tempWeights[i] = this.weights[i] + delta;
			} else {
				tempWeights[i] = 0;
			}
		}
		return Math.tanh(this.evaluateBoard(board, this.myPiece, tempWeights));
	}

	private void calcDiff() {
		for (int i = 0; i < states.size() - 1; i++) {
			diff.add(reward(states.get(i + 1), weights) - reward(states.get(i), weights));
		}
		diff.add(reward(board) - reward(states.get(states.size() - 1), weights));
	}

	private double firstSum(int weightIndex) {
		double sum = 0;
		double delta = 0.0000001;

		for (int i = 0; i < states.size() - 1; i++) {
			sum += ((reward(states.get(i), weightIndex, delta) - reward(states.get(i), weightIndex, 0.0)) / delta)
					* secondSum(i);
		}
		return sum;
	}

	private double secondSum(int i) {
		double sum = 0;
		double lamda = 0.95;
		for (int j = i; j < states.size() - 1; j++) {
			sum += Math.pow(lamda, (j - i)) * diff.get(i);
		}
		return sum;
	}

	private void clearTD() {
		states.clear();
		diff.clear();
	}

	private void printWeights() {
		List<String> lines = Arrays.asList(Arrays.toString(weights));
		Path file = Paths.get("Weights.txt");
		try {
			// Files.write(file, lines, Charset.forName("UTF-8"));
			Files.write(file, lines, Charset.forName("UTF-8"), StandardOpenOption.APPEND);
		} catch (IOException e) {
			e.printStackTrace();
			System.exit(0);
		}
	}

}
