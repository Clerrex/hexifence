import java.io.PrintStream;
import java.util.Scanner;

import aiproj.hexifence.Move;
import aiproj.hexifence.Piece;
import aiproj.hexifence.Player;
import aiproj.hexifence.mlumley.Board;
import aiproj.hexifence.mlumley.Coordinates;

public class HumanPlayer implements Piece, Player {

	int piece = 0;
	int oPiece = 0;
	Board board = null;
	boolean validMove = true;
	Scanner sc = new Scanner(System.in);

	@Override
	public int init(int n, int p) {

		board = new Board(n);

		if (p == 1) {
			piece = Piece.BLUE;
			oPiece = Piece.RED;
		} else if (p == 2) {
			piece = Piece.RED;
			oPiece = Piece.BLUE;
		} else {
			return -1;
		}

		board.createBoard();
		// board.createTiles();

		return 0;
	}

	@Override
	public Move makeMove() {

		int row = 0;
		int col = 0;

		System.out.println("Enter row, col");
		row = sc.nextInt();
		col = sc.nextInt();

		Move move = new Move();
		move.P = piece;
		move.Row = row;
		move.Col = col;

		board.setChar(move, piece);

		board.didCapture(new Coordinates(move.Row, move.Col), piece);

		return move;
	}

	@Override
	public int opponentMove(Move m) {

		if (board.getValue(m) == '+') {
			try {
					board.setChar(m, oPiece);
			} catch (Exception e) {
				System.out.println("ERROR");
				System.exit(0);
			}

			if (board.didCapture(new Coordinates(m.Row, m.Col), oPiece)) {
				return 1;
			}

			return 0;
		}
		validMove = false;
		return -1;
	}

	@Override
	public int getWinner() {
		int winner = 0;

		if (!validMove) {
			return Piece.INVALID;
		} else if ((winner = board.findWinner()) > 0) {
			return winner;
		}
		return 0;
	}

	@Override
	public void printBoard(PrintStream output) {
		for (char[] line : board.getBoard()) {
			for (char ch : line) {
				System.out.print(ch + " ");
			}
			System.out.print("\n");
		}
	}

}
