/*
 * COMP30024 Artificial Intelligence
 * HexiFence
 * Authors: Bikram Bora (bbora) and Michael Lumley (mlumley)
 */
package aiproj.hexifence.mlumley;

import java.util.ArrayList;

/**
 * Represents a hexagon on the board
 */
public class Tile {

	private ArrayList<Coordinates> points = new ArrayList<Coordinates>();
	private Coordinates center = null;
	private Board board = null;

	public Tile(Board board, Coordinates start) {
		this.board = board;
		// First row
		points.add(start);
		points.add(new Coordinates(start.getX() + 1, start.getY()));
		// Second row
		points.add(new Coordinates(start.getX(), start.getY() + 1));
		points.add(new Coordinates(start.getX() + 2, start.getY() + 1));
		// Third row
		points.add(new Coordinates(start.getX() + 1, start.getY() + 2));
		points.add(new Coordinates(start.getX() + 2, start.getY() + 2));
		this.center = new Coordinates(getStart().getX() + 1, getStart().getY() + 1);
	}

	/**
	 * @return the center of the tile
	 */
	public Coordinates getCenter() {
		return center;
	}

	/**
	 * @return the top-left point
	 */
	public Coordinates getStart() {
		return points.get(0);
	}

	/**
	 * @return the points of the tile
	 */
	public ArrayList<Coordinates> getPoints() {
		return points;
	}

	/**
	 * Checks if the tile contains a point
	 * 
	 * @param point1
	 *            the point
	 * @return True if it contains the point or False if it doesn't
	 */
	public boolean containsPoint(Coordinates point1) {
		for (Coordinates point2 : points) {
			if (point1.equals(point2)) {
				return true;
			}
		}
		return false;

	}

	/**
	 * @return the number of free edges in the tile
	 */
	public int numOpenEdges() {
		int n = 0;

		for (Coordinates point : points) {
			if (board.getBoard()[point.getX()][point.getY()] == '+') {
				n++;
			}
		}
		return n;
	}

}
