/*
 * COMP30024 Artificial Intelligence
 * HexiFence
 * Authors: Bikram Bora (bbora) and Michael Lumley (mlumley)
 */
package aiproj.hexifence.mlumley;

import java.util.ArrayList;
import java.util.Arrays;

import aiproj.hexifence.Move;
import aiproj.hexifence.Piece;

/**
 * Represents a HexiFence board Hold the current state of the board and the
 * dimension
 */
public class Board {

	private int dimension;
	private int n = 0;
	private char[][] board;
	private ArrayList<Tile> tiles = new ArrayList<Tile>();

	public Board(int n) {
		this.dimension = n;
		// The length and height of the board
		this.n = 4 * dimension - 1;
		board = new char[this.n][this.n];
	}

	// TDLeaf
	public Board(Board aBoard) {
		char[][] board = new char[aBoard.getLength()][aBoard.getLength()];
		for (int i = 0; i < aBoard.getLength(); i++) {
			board[i] = Arrays.copyOf(aBoard.getBoard()[i], aBoard.getLength());
		}
		this.n = aBoard.getLength();
		this.dimension = aBoard.getDimension();
		this.board = board;
		this.createTiles();
	}

	/**
	 * @return the board
	 */
	public char[][] getBoard() {
		return board;
	}

	/**
	 * @return the tiles
	 */
	public ArrayList<Tile> getTiles() {
		return tiles;
	}

	/**
	 * @return the dimension
	 */
	public int getDimension() {
		return dimension;
	}

	/**
	 * 
	 * @return the length
	 */
	public int getLength() {
		return n;
	}

	/**
	 * Get the value at a position on the board
	 * 
	 * @param move
	 *            the position
	 * @return the value at the position
	 */
	public char getValue(Move move) {
		return board[move.Row][move.Col];
	}

	/**
	 * Get the value at a position on the board
	 * 
	 * @param i
	 *            the row
	 * @param j
	 *            the column
	 * @return the value at the position
	 */
	public char getValue(int i, int j) {
		return board[i][j];
	}

	/**
	 * Claims a position on the board for a player
	 * 
	 * @param move
	 *            the position
	 * @param piece
	 *            the player making the move
	 */
	public void setChar(Move move, int piece) {
		if (piece == Piece.BLUE)
			board[move.Row][move.Col] = 'B';
		else
			board[move.Row][move.Col] = 'R';
	}

	/**
	 * Undoes a move, setting it back to an open edge
	 * 
	 * @param move
	 *            the move to be undone
	 */
	public void undoMove(Move move) {
		board[move.Row][move.Col] = '+';
		for (Tile tile : tiles) {
			if (tile.containsPoint(new Coordinates(move.Row, move.Col)) && tile.numOpenEdges() == 1) {
				board[tile.getCenter().getX()][tile.getCenter().getY()] = '-';
			}
		}
	}

	/**
	 * Creates an empty board
	 */
	public void createBoard() {
		int i = 0;
		int j = 0;
		int left_margin = 0;
		int right_margin = 2 * dimension;
		boolean plus = true;
		boolean plusRow = true;

		for (i = 0; i < n; i++) {
			for (j = 0; j < n; j++) {
				// Top row
				if (i == 0 && j < right_margin) {
					board[i][j] = '+';
				}
				// Bottom row
				else if (i == n - 1 && j >= left_margin) {
					board[i][j] = '+';
				}

				else if ((i != 0 && i != n - 1) && j < right_margin && j >= left_margin && plusRow) {
					board[i][j] = '+';
				}

				else if ((i != 0 && i != n - 1) && j < right_margin && j >= left_margin && plus && !plusRow) {
					board[i][j] = '+';
					plus = false;
				} else {
					board[i][j] = '-';
					plus = true;
				}
			}

			right_margin++;
			if (right_margin > n) {
				left_margin++;
			}
			plus = true;
			plusRow = plusRow ? false : true;
		}
		createTiles();
	}

	/**
	 * Creates the tiles on the board
	 */
	private void createTiles() {
		int count = 0;
		int innerStart = 0;
		int innerEnd = dimension;

		for (int i = 0; i < (2 * dimension - 1); i++) {
			for (int j = innerStart; j < innerEnd; j++) {
				tiles.add(new Tile(this, new Coordinates(j * 2, i * 2)));
			}
			count++;
			if (count >= dimension) {
				innerStart++;
			} else {
				innerEnd++;
			}
		}
	}

	/**
	 * Finds the winner of a match
	 * 
	 * @return the winner as a int
	 */
	public int findWinner() {
		int red = 0;
		int blue = 0;
		boolean finished = false;

		for (Tile tile : tiles) {
			if (board[tile.getCenter().getX()][tile.getCenter().getY()] == 'r') {
				red++;
			} else if (board[tile.getCenter().getX()][tile.getCenter().getY()] == 'b') {
				blue++;
			} else if (board[tile.getCenter().getX()][tile.getCenter().getY()] == '-') {
				return 0;
			}
			finished = true;
		}

		if (finished) {
			if (red > blue) {
				return Piece.RED;
			} else if (blue > red) {
				return Piece.BLUE;
			} else if (red == blue) {
				return Piece.DEAD;
			}
		}
		return 0;
	}

	/**
	 * Checks if a move will capture any tiles. If it does the tile is captured.
	 * 
	 * @param move
	 *            the move
	 * @param piece
	 *            the player making the move
	 * @return True if one or two tiles are captured or False if no tiles are
	 *         captured
	 */
	public boolean didCapture(Coordinates move, int piece) {
		boolean captured = false;

		for (Tile tile : tiles) {
			if (tile.containsPoint(move) && tile.numOpenEdges() == 0) {
				if (piece == Piece.RED)
					board[tile.getCenter().getX()][tile.getCenter().getY()] = 'r';
				else
					board[tile.getCenter().getX()][tile.getCenter().getY()] = 'b';
				captured = true;
			}
		}
		return captured;
	}

}
