import aiproj.hexifence.Move;
import aiproj.hexifence.mlumley.Board;

/**
 * Contains a board, its score and the last move
 */
public class State {

	private double score;
	private Board board;
	private Move move;

	public State(Board board, double score, Move move) {
		this.board = board;
		this.score = score;
		this.move = move;
	}

	public double getScore() {
		return score;
	}

	public Board getBoard() {
		return board;
	}

	public Move getMove() {
		return move;
	}

}
