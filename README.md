# Hexifence #

### What is this repository for? ###

Hexifance is a board game similar to Dots and Boxes, from wikipedia:

Starting with an empty grid of dots, two players take turns adding a single horizontal or vertical line between two unjoined adjacent dots.
The player who completes the fourth side of a 1×1 box earns one point and takes another turn. The game ends when no more lines can be placed.
The winner is the player with the most points.

However in Hexifence, instead of boxes we use hexagons and instead of people we have AIs.

There are a few different AI that can be used the worst being the RandomPlayer and the best
MLumleyTDLeafKiller.

### How do I get set up? ###

Simply import the source files and run Referee with the following arguments:
1. The board dimension.
2. The full class name of the implementation of the Player class of the first player, i.e.,
including package name. This instance would play as BLUE.
3. The full class name of the implementation of the Player class of the second player, i.e.,
including package name. This instance would play as RED.